import random
import matplotlib.pyplot as plt


# Для массивов данных
def binary_search(data, val):
    low = 0
    high = len(data) - 1
    mid = 0
    counter = 0
    while low <= high:
        counter += 1
        mid = (high + low) // 2

        if data[mid] < val:
            counter += 1
            low = mid + 1

        elif data[mid] > val:
            counter += 1
            high = mid - 1

        else:
            return counter

    return -1


def linear_search(data, val):
    counter = 0
    value_found = False
    for i in range(len(data)):
        counter += 1
        if data[i] == val:
            value_found = True
            return counter
    if not value_found:
        return counter

# Для строк
def naive(data, substring):
    counter = 1
    substring_index = data.find(substring)
    if substring_index == -1:
        return counter
    else:
        counter += substring_index
        return counter


class KMP:
    def partial(self, pattern):
        ret = [0]

        for i in range(1, len(pattern)):
            j = ret[i - 1]
            while j > 0 and pattern[j] != pattern[i]:
                j = ret[j - 1]
            ret.append(j + 1 if pattern[j] == pattern[i] else j)
        return ret

    def search(self, data, substring):
        counter = 0
        partial, j = self.partial(substring), 0

        for i in range(len(data)):
            counter += 1
            while j > 0 and data[i] != substring[j]:
                j = partial[j - 1]
            if data[i] == substring[j]:
                j += 1
            if j == len(substring):
                return counter
        return counter


counter_binary = []
counter_linear = []
counter_naive = []
counter_kmp = []

letters = 'abcdefghijklmnopqrstuvwxyz'
sizes = []

for i in range(1, 5000):
    sizes.append(i)
    data = [random.randint(1, 100) for n in range(i)]
    item = random.choice(data)
    counter_binary.append(binary_search(data, item))

    data = [random.randint(1, 100) for n in range(i)]
    item = random.choice(data)
    counter_linear.append(linear_search(data, item))

    data_string = ''.join(random.choice(letters) for n in range(i))
    item = random.choice(data_string)
    counter_naive.append(naive(data_string, item))

    data_string = ''.join(random.choice(letters) for n in range(i))
    item = random.choice(data_string)
    counter_kmp.append(KMP().search(data=data_string, substring=item))


plt.plot(sizes, counter_binary)
plt.title('Бинарный поиск')
plt.xlabel('Размерность')
plt.ylabel('Количество операций')
plt.show()

plt.plot(sizes, counter_linear)
plt.title('Линейный поиск')
plt.xlabel('Размерность')
plt.ylabel('Количество операций')
plt.show()

plt.plot(sizes, counter_naive)
plt.title('Наивный поиск')
plt.xlabel('Размерность')
plt.ylabel('Количество операций')
plt.show()

plt.plot(sizes, counter_kmp)
plt.title('Поиск КМП')
plt.xlabel('Размерность')
plt.ylabel('Количество операций')
plt.show()
