# Для массивов данных
def binary_search(data, val):
    low = 0
    high = len(data) - 1
    mid = 0
    counter = 0
    while low <= high:
        counter += 1
        mid = (high + low) // 2

        if data[mid] < val:
            counter += 1
            low = mid + 1

        elif data[mid] > val:
            counter += 1
            high = mid - 1

        else:
            print('Значение найдено по индексу ', mid)
            print('Количество операций - ', counter)
            return mid
    print('Значение не найдено')
    return -1


def linear_search(data, val):
    counter = 0
    value_found = False
    for i in range(len(data)):
        counter += 1
        if data[i] == val:
            print('Значение найдено по индексу ', i)
            value_found = True
            print('Количество операций - ', counter)
            return i
    if not value_found:
        print('Значение не найдено')
        print('Количество операций - ', counter)
        return -1


# Для строк
def naive(data, substring):
    counter = 1
    substring_index = data.find(substring)
    if substring_index == -1:
        print('Подстрока не найдена')
    else:
        print('Подстрока найдена по индексу ', substring_index)
        counter += substring_index
        print('Количество операций - ', counter)
    return substring_index


class KMP:
    def partial(self, pattern):
        ret = [0]

        for i in range(1, len(pattern)):
            j = ret[i - 1]
            while j > 0 and pattern[j] != pattern[i]:
                j = ret[j - 1]
            ret.append(j + 1 if pattern[j] == pattern[i] else j)
        return ret

    def search(self, data, substring):
        counter = 0
        partial, j = self.partial(substring), 0

        for i in range(len(data)):
            counter += 1
            while j > 0 and data[i] != substring[j]:
                j = partial[j - 1]
            if data[i] == substring[j]:
                j += 1
            if j == len(substring):
                print('Подстрока найдена по индексу ', i - (j - 1))
                print('Количество операций - ', counter)
                return i - (j - 1)
        print('Подстрока не найдена')
        return -1


if __name__ == '__main__':
    input_type = int(input('Выберите желаемый формат вводных данных\n1 - массив, 2 - строка '))
    if input_type == 1:
        data = input('Введите данные, разделяя их пробелами ')
        data = data.split(' ')
        for item_id in range(len(data)):
            data[item_id] = float(data[item_id])
        search_item = float(input('Введите искомые данные '))

        method = int(input('Выберите желаемый метод поиска\n1 - линейный, 2 - бинарный '))
        if method == 1:
            linear_search(data, search_item)
        elif method == 2:
            binary_search(data, search_item)
        else:
            print('Несуществующий вариант')
            exit()
    elif input_type == 2:
        data = input('Введите данные ')
        search_item = input('Введите искомые данные ')
        method = int(input('Выберите желаемый метод поиска\n1 - наивный, 2 - К-М-П '))
        if method == 1:
            naive(data, search_item)
        elif method == 2:
            KMP().search(data=data, substring=search_item)
        else:
            print('Несуществующий вариант')
            exit()
    else:
        print('Несуществующий вариант')
        exit()
